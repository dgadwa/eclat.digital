<?php
function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
 
	// example of adding a script file
	//wp_enqueue_script( 'name-me', get_template_directory_uri() . '/assets/scripts/scriptsDG/the-file.js', array('jquery'), '1.2 this is optional', true loads before </body> false loads before </head> );
 
        
    // Adding scripts file in the footer
    wp_enqueue_script( 'site-js', get_template_directory_uri() . '/assets/scripts/scripts.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true );		

	// set up Fonts.com 
	wp_enqueue_style( 'fast-font-css', '//fast.fonts.net/cssapi/2dcdb365-9621-4a68-a32d-7750b5c85779.css', array(), '', 'all' );	
	
	// set up animate.css 
	wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/assets/styles/css/animate.min.css', array('fast-font-css'), '', 'all' );
	
	// Google fonts
	wp_enqueue_style( 'fonts-css', 'https://fonts.googleapis.com/css?family=Roboto+Slab:400,700', array('fast-font-css'), '', 'all' );
	
	//Fancybox3
	wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/assets/styles/css/jquery.fancybox.min.css', array('fonts-css'), '', 'all' );

    // Register main stylesheet
    wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/styles/style.css', array('fonts-css'), filemtime(get_template_directory() . '/assets/styles/scss'), 'all' );

    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);













