<?php
/*
Template name: Full Width Split Partner
*/

get_header(); ?>

	<?php 
	// add the partial file with the code to create a banner to this template	
	get_template_part( 'parts/components/component', 'banner' ); 
	?>

	<div class="content grid-container">
	
		<div class="inner-content grid-x grid-margin-x grid-padding-x">
	
		    <main class="main small-12 medium-12 large-12 cell" role="main">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php get_template_part( 'parts/loop', 'page' ); ?>
					
					
					<div class="grid-x full-width-split">
						<div class="cell small-12 large-6 left-content-wrap">
							<div class="grid-x grid-padding-x align-middle left-content">
								<div class="cell">
									<?php the_field ('left_content')  ?>
								</div>
							</div>	
						</div>
		
						<div class="cell small-12 large-6 right-content-wrap">
							<div class="grid-x grid-padding-x align-middle right-content">
								<div class="cell">
									<?php the_field ('right_content')  ?>
								</div>
							</div>					
						</div>
					</div> <!-- grid-x end -->
					

									<h2> Under The Hood </h2>
									<?php the_field( 'content_area' ); ?>	

					
					<div class="grid-container partner-logo-container">
						<div class="grid-x align-middle">
							<?php if ( have_rows( 'fw_partner_logos' ) ): ?>
								<?php while ( have_rows( 'fw_partner_logos' ) ) : the_row(); ?>
									<?php if ( get_row_layout() == 'partner_logos' ) : ?>
<!--
										<h2 class="partner-logo-heading">
											<?php the_sub_field( 'partner_logos_heading' ); ?>
										</h2>
-->	
										<!-- Partner logo cells begin -->
										<?php $partner_logo_1 = get_sub_field( 'partner_logo_1' ); ?>
										<div class="cell partner-logo small-12 medium-4">
											<?php if ( $partner_logo_1 ) { ?>
												<img src="<?php echo $partner_logo_1['url']; ?>" alt="<?php echo $partner_logo_1['alt']; ?>" />
											<?php } ?>
											<?php $partner_logo_2 = get_sub_field( 'partner_logo_2' ); ?>
										</div>	
										<div class="cell partner-logo small-12 medium-4">	
											<?php if ( $partner_logo_2 ) { ?>
												<img src="<?php echo $partner_logo_2['url']; ?>" alt="<?php echo $partner_logo_2['alt']; ?>" />
											<?php } ?>
											<?php $partner_logo_3 = get_sub_field( 'partner_logo_3' ); ?>
										</div>	
										<div class="cell partner-logo small-12 medium-4">	
											<?php if ( $partner_logo_3 ) { ?>
												<img src="<?php echo $partner_logo_3['url']; ?>" alt="<?php echo $partner_logo_3['alt']; ?>" />
											<?php } ?>
										</div>	
									<?php endif; ?>
								<?php endwhile; ?>
							<?php else: ?>
								<?php // no layouts found ?>
							<?php endif; ?>
							<!-- Partner logo cells end -->
						</div> <!-- grid-x -->			
					</div> <!-- end grid-container --> 
					
					<?php // get_template_part('parts/components/component', 'slider');?>

				<?php endwhile; endif; ?>							

			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->
	
	</div> <!-- end #content -->
	
<?php get_footer(); ?>
