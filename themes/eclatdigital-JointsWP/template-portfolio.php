<?php
/*
Template Name: Portfolio
*/

get_header(); ?>

	<?php 
	// add the partial file with the code to create a banner to this template	
	get_template_part( 'parts/components/component', 'banner' ); 
	?>

	<div class="content grid-container">
	
		<div class="inner-content grid-x grid-margin-x grid-padding-x">
	
		    <main class="main small-12 medium-12 large-12 cell" role="main">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<?php get_template_part( 'parts/loop', 'page' ); ?>
					
					<?php 
						$smallUp = get_field('small_up');
						$mediumUp = get_field('medium_up');
						$xmediumUp = get_field('x-medium_up');
						$largeUp = get_field('large_up');
						$images = get_field('portfolio_gallery');
			

						if( $images ): ?>
							<!-- Fancybox 3: http://fancyapps.com/fancybox/3/docs/ -->
						    <div class="grid-x grid-padding-x small-up-<?=$smallUp;?> medium-up-<?=$mediumUp;?> xmedium-up<?=$xmediumUp;?> large-up-<?=$largeUp;?>">
						        <?php $mycounter=0; foreach( $images as $image ): $mycounter++; 
							        $projectLink = $image['description'];
						        ?>
						            <div class="cell hover-image portfolio-caption">
						                <a href="#inlineFancy-<?=$mycounter;?>" data-fancybox="cl-group" data-caption="<?php echo $image['caption']; ?>"> 
						                     <img src="<?php echo $image['sizes']['medium']; ?>" width="<?php echo $image['width']; ?>" height="<?php echo $image['height']; ?>"
						                     alt="<?php echo $image['alt']; ?>" />
						                </a>
						                <!-- Image caption for bottom of Fancybox -->
						                <p><?php echo $image['caption']; ?></p>
						                    
										
										<div id="inlineFancy-<?=$mycounter;?>" class="inlineFancy" style="display: none;">
<!-- 											PHP BRAIN TWISTER -->
											<?php if($projectLink):?>
												<a href="<?=$projectLink;?>" target="_blank">
													<img src="<?php echo $image['url']; ?>" width="<?php echo $image['width']; ?>" height="<?php echo $image['height']; ?>"
						                     alt="<?php echo $image['alt']; ?>" />
												</a>
											<?php else: ?>
												<a href="#">
													<img src="<?php echo $image['url']; ?>" width="<?php echo $image['width']; ?>" height="<?php echo $image['height']; ?>"
						                     alt="<?php echo $image['alt']; ?>" />
												</a>	
											<?php endif; ?>
										</div>
						            
						            </div>
						        <?php endforeach; ?>
						    </div>
						<?php endif; ?> 
					
						<?php // get_template_part('parts/components/component', 'slider');?>


				<?php endwhile;
					wp_reset_postdata ();
				?> 
					
			<?php endif; ?>							
				
			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->
	
	</div> <!-- end #content -->

<?php get_footer(); ?>
