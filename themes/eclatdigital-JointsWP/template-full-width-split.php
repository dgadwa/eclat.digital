<?php
/*
Template name: Full Width Split
*/

get_header(); ?>

	<?php 
	// add the partial file with the code to create a banner to this template	
	get_template_part( 'parts/components/component', 'banner' ); 
	?>

	<div class="content grid-container">
	
		<div class="inner-content grid-x grid-margin-x grid-padding-x">
	
		    <main class="main small-12 medium-12 large-12 cell" role="main">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php get_template_part( 'parts/loop', 'page' ); ?>
					
					
					<div class="grid-x full-width-split" >

						<div class="cell small-12 large-6 left-content-wrap">
							<div class="grid-x grid-padding-x align-middle left-content">
								<div class="cell">
									<?php the_field ('left_content')  ?>
								</div>
							</div>	
						</div>
		
						<div class="cell small-12 large-6 right-content-wrap">
							<div class="grid-x grid-padding-x align-middle right-content">
								<div class="cell">
									<?php the_field ('right_content')  ?>
								</div>
							</div>
						
						</div>
					</div>

					
					<?php // get_template_part('parts/components/component', 'slider');?>

				<?php endwhile; endif; ?>							

			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->
	
	</div> <!-- end #content -->

	
	
<?php get_footer(); ?>
