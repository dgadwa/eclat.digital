<section id="page-header">
<?php 
// set the acf fields to avariable to make them easier to work with below
$bannerImg = get_field('banner_image');
$bannerContent = get_field('banner_content');
$bannerHeight = get_field('banner_min_height');
$bannerPosition = get_field('background_position');
$hideTitle = get_field('hide_page_title');
?>

<?php if ($bannerImg):?>
	<?php $ctr = 0; foreach($bannerImg  as $image ): $ctr++; ?>							 
		<?php $bannerimg[$ctr] = $image['url']; ?>					
	<?php endforeach; ?>
<?php endif;?>	

<div class="marketing-site-hero" <?php if ($bannerImg):?> data-interchange="[<?=$bannerimg[3];?>, small], [<?=$bannerimg[2];?>, medium], [<?=$bannerimg[1];?>, large]" style="background-position: <?=$bannerPosition;?>" <?php endif;?>>
	<div class="grid-container grid-container-padded">
	<div class="grid-x grid-margin-x grid-padding-x align-middle marketing-site-hero-content" style="min-height: <?=$bannerHeight;?>;">
		<div class="cell small-12 bannerText" style="min-height: <?=$bannerHeight;?>;">
<!--
			<h1 class="tlt">
				<ul class="texts">
					<li data-out-effect="fadeOut" data-out-shuffle="true">Some Title</li>
					<li data-in-effect="fadeIn">Another Title</li>
				</ul>
			</h1>
-->

			<?php // if ( $hideTitle ) :?>
			<!-- <h1 class="blurb blurb-title"><ul class="texts"> <?php the_title();?> </h1> -->
			<?php // endif;?>
			<?php if ( $bannerContent ):?>
				<?=$bannerContent;?>	
			<?php endif;?>
		</div>		
	</div>
	</div>

</div>
	
</section>
