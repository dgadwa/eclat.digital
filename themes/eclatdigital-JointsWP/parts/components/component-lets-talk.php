<?php

// check if the repeater field has rows of data
if( have_rows('bottom_nav_links','options') ) :?>

<div class="grid-x menu bottomNav align-center align-middle">
 	<?php // loop through the rows of data
    while ( have_rows('bottom_nav_links','options') ) : the_row();
        // display a sub field value
        $linkT = get_sub_field ('link_text','options');
		$link = get_sub_field ('link','options');
		$icon = get_sub_field ('icon','options');
		$iclass = get_sub_field ('class','options');
		?>


			<div class="cell shrink <?=$iclass;?>">
				<?php if ( $link ):?>
					<a href=" <?=$link;?>">
						<?php if ($linkT):?>
							<?=$linkT;?>
						<?php endif;?>
						<?php if ($icon):?>
							<?=$icon;?>
						<?php endif;?>
					</a>
				<?php else: // shorthand php example?>
					<?php if ( $linkT ) {
						echo $linkT;
					}?>
					<?php if ($icon) {
						echo $icon;
					}?>
				<?php endif;?>
					
			</div>

	

	<?php 		
    endwhile; ?>
</div>

<?php else :

    // no rows found

endif;

?>