<?php // slides	 setup					
$slides = get_field('slides');
?>	
<?php if ($slides):?>

<div id="myFlickity">
	<?php foreach( $slides as $slide ):?>
		<div class="slide">
		  <img src="<?=$slide['url'];?>" alt="<?=$slide['url'];?>" class="slide" />
		</div>
	<?php endforeach;?>
</div>	
<?php endif;?>