<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/responsive-navigation/
 */
?>

<div class="top-bar" data-sticky-container id="main-menu">
	<div class="sticky" data-sticky data-margin-top="0">
		<div class="top-bar-left">
			<ul class="menu">
				<li><a href="#"><img src="wp-content/uploads/2017/08/éclat.digital-mobile-32px-2x.png"/></a></li> 
				<li><a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a></li>
			</ul>
		</div>		
		<div class="top-bar-right">
			<?php joints_top_nav(); ?>
		</div>
	</div>
</div>