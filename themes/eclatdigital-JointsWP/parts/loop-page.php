<?php
/**
 * Template part for displaying page content in page.php
 */
$hideTitle = get_field('hide_page_title');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
						
<?php 
	// this can alse use !$hideTitle or $hideTitle == false or $hideTitle !== true
	if ( $hideTitle  == 0 )	:?>
	<header class="article-header">
		<h1 class="page-title"><?php the_title(); ?></h1>
	</header> <!-- end article header -->
<?php endif;?>					
    <section class="entry-content" itemprop="articleBody">
	    <?php the_content(); ?>
	</section> <!-- end article section -->
						
	<footer class="article-footer">
		 <?php wp_link_pages(); ?>
	</footer> <!-- end article footer -->
						    
	<?php comments_template(); ?>
					
</article> <!-- end article -->