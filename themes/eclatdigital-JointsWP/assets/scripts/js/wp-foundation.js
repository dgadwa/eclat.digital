/*
These functions make sure WordPress
and Foundation play nice together.
*/

jQuery(document).ready(function() {
	
    // Remove empty P tags created by WP inside of Accordion and Orbit
    jQuery('.accordion p:empty, .orbit p:empty').remove();

	// Adds Flex Video to YouTube and Vimeo Embeds
	jQuery('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
		if ( jQuery(this).innerWidth() / jQuery(this).innerHeight() > 1.5 ) {
		  jQuery(this).wrap("<div class='widescreen responsive-embed'/>");
		} else {
		  jQuery(this).wrap("<div class='responsive-embed'/>");
		}
	});
	// Add scrips here that you want to load when the DOM is done	
	// Joshua's Badass Breakpoint Script
	debugin();
	myslides();
	acfForms();
	txtAnimateToggle();
	//loadMorphs();
});




// This right here toggles the blurb text on and off in mobile
function txtAnimateToggle() {
	// 	declaring variables to store in browsers memory divide multiple vars with a , end group with ;
	//console.log('txtAnimateToggle just droped the beat' );
	var subItemTarget = jQuery('ul.menu.super-kick-ass .menu-item.menu-item-has-children.is-dropdown-submenu-parent, ul.menu.super-kick-ass .menu-item.menu-item-has-children.is-dropdown-submenu-parent > ul.submenu'),
		blurb = jQuery('.blurb-wrapper'),
		burgerIcon = jQuery('.title-bar .menu-icon');
		
	// select menu-icon and triggle class toggle on .blurb-wrapper when it is Clicked		
	burgerIcon.on('click', function(){
		// console.log('this function fired when you clicked me');
		blurb.toggleClass('txtAnimate');
	});
	// select menu items with sub-menus and toggle class on .blurb-wrapper when it is Hovered

	subItemTarget.mouseenter(function(){
		//console.log('mouse entered the target');
		blurb.addClass('txtAnimate');
	}).mouseleave(function(){
		//console.log('mouse left the target');
		blurb.removeClass('txtAnimate');
	});	
}


function debugin() {
	var currentBreakpoint = Foundation.MediaQuery.current;
	console.log('the current breakpoint is ' + currentBreakpoint);
	jQuery(window).resize(function() {
		var currentBreakpoint = Foundation.MediaQuery.current;
		console.log('the current breakpoint is ' + currentBreakpoint);
	});
}

function myslides() {
	// console.log('flickity is init yo');
	jQuery('#myFlickity').flickity({
	
	// options
	cellSelector: '.carousel-cell',
	cellAlign: 'left',
	contain: true,
	
	});
	
}

function acfForms() {
	//console.log('form time b');

	//add inline style to an element
	jQuery('div.af-submit.acf-form-submit').css({'padding': '15px 12px'});

	// add foundation classes to this buttom
	jQuery('button.acf-button.af-submit-button').addClass('button expanded large');
}

// loads animation functions for morphext http://morphext.fyianlai.com/
function loadMorphs() {
	
	jQuery("#js-rotating").Morphext({
	    // The [in] animation type. Refer to Animate.css for a list of available animations.
	    animation: "fadeInLeft",
	    // An array of phrases to rotate are created based on this separator. Change it if you wish to separate the phrases differently (e.g. So Simple | Very Doge | Much Wow | Such Cool).
	    separator: ",",
	    // The delay between the changing of each phrase in milliseconds.
	    speed: 2000,
	    complete: function () {
	        // Called after the entrance animation is executed.
	    }
	});
	
	jQuery("#js-machinez").Morphext({
	    // The [in] animation type. Refer to Animate.css for a list of available animations.
	    animation: "fadeInLeft",
	    // An array of phrases to rotate are created based on this separator. Change it if you wish to separate the phrases differently (e.g. So Simple | Very Doge | Much Wow | Such Cool).
	    separator: ",",
	    // The delay between the changing of each phrase in milliseconds.
	    speed: 3000,
	    complete: function () {
	        // Called after the entrance animation is executed.
	    }
	});	
}
