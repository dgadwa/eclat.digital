<?php
/**
 * The template for displaying the header
 *
 * This is the template that displays all of the <head> section
 *
 */
?>

<!doctype html>

  <html class="no-js"  <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">
		
		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta class="foundation-mq">
		
		<!-- If Site Icon isn't set in customizer -->
		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
			<!-- Icons & Favicons -->
			<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
			<link href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-icon-touch.png" rel="apple-touch-icon" />	
	    <?php } ?>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		
		<!-- Chrome theme color -->
		<meta name="theme-color" content="#271c25">

		<!-- Fonts.com CSS Publish code -->
		<!-- 	moved to enqueue file in functions	
		<link type="text/css" rel="stylesheet" href="//fast.fonts.net/cssapi/2dcdb365-9621-4a68-a32d-7750b5c85779.css"/> -->

		<!-- Animate.css -->
		<!-- moved this to equeue file in functions <link rel="stylesheet" href="/wp-content/themes/xygrid-yo/assets/styles/css/animate.min.css"> -->
		
		<!-- Font Awesome -->
		<!-- being enqueued by acf plugin on pages that use font-awesome field 
  		<script defer src="/wp-content/themes/xygrid-yo/assets/scripts/js/packs/solid.js"></script>
  		<script defer src="/wp-content/themes/xygrid-yo/assets/scripts/js/packs/brands.js"></script>
  		<script defer src="/wp-content/themes/xygrid-yo/assets/scripts/js/fontawesome.js"></script>
  		-->
		

		<!-- Scroll Reveal stuff -->
		<!-- <script defer src="/wp-content/themes/xygrid-yo/node_modules/scrollreveal/dist/scrollreveal.js"></script>
		<script defer src="/wp-content/themes/xygrid-yo/assets/scripts/customJavascriptDG/scrollRevealDG.js"></script> -->
			


		<?php wp_head(); ?>

	</head>
			
	<body <?php body_class(); ?>>

		<div class="off-canvas-wrapper">
			
			<!-- Load off-canvas container. Feel free to remove if not using. -->			
			<?php get_template_part( 'parts/content', 'offcanvas' ); ?>
			
			<div class="off-canvas-content" data-off-canvas-content>
				<div data-sticky-container>
					<div class="sticky" data-sticky data-margin-top="0">
						<header class="header" role="banner">
							<div class="grid-container">
							 <!-- This navs will be applied to the topbar, above all content 
								  To see additional nav styles, visit the /parts directory -->
							 <?php get_template_part( 'parts/nav', 'title-bar' ); ?>
							</div>
						</header> <!-- end .header -->
					</div>
				</div>		