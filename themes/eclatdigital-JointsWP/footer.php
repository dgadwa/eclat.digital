<?php
/**
 * The template for displaying the footer. 
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */			
 ?>

<?php 
$hideBottomNav = get_field('hide_bottom_navigation');	
$hideFooter = get_field('hide_footer_bar');

// check if hide_bottom_nav is not selected or does not have a value of true  by using ! before the get_field('field_name')	

//HIDE BOTTOM NAV
if ( !$hideBottomNav ) :?>
	<?php 
	get_template_part('parts/components/component', 'lets-talk');?>
<?php endif;?>	

<!--HIDE FOOTER -->
<?php if ( !$hideFooter == true ): ?>

				<footer class="footer" role="contentinfo">
					<div class="grid-container">
						<div class="inner-footer grid-x grid-margin-x grid-padding-x grid-padding-y">
							
							<div class="small-12 medium-12 large-12 cell joints-footer-nav">
								<nav role="navigation">
		    						<?php joints_footer_links(); ?>
		    					</nav>
		    				</div>
							
							<div class="small-12 medium-12 large-12 cell">
								<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
							</div>
						
						</div> <!-- end #inner-footer -->
					</div>
				</footer> <!-- end .footer -->
<?php endif;?>			
			</div>  <!-- end .off-canvas-content -->
					
		</div> <!-- end .off-canvas-wrapper -->
		
		<?php wp_footer(); ?>
		
	</body>
	
</html> <!-- end page -->